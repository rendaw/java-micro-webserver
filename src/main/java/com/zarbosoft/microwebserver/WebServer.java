package com.zarbosoft.microwebserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zarbosoft.checkjson.ValidationError;
import com.zarbosoft.coroutines.Coroutine;
import com.zarbosoft.coroutinescore.SuspendExecution;
import com.zarbosoft.rendaw.common.Assertion;
import com.zarbosoft.rendaw.common.Common;
import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.UndertowOptions;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.RequestBufferingHandler;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;
import io.undertow.util.Methods;
import io.undertow.websockets.WebSocketConnectionCallback;
import io.undertow.websockets.core.*;
import io.undertow.websockets.spi.WebSocketHttpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xnio.XnioWorker;

import java.io.*;
import java.lang.reflect.Method;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import static com.zarbosoft.rendaw.common.Common.uncheck;

/**
 * A micro webserver utility
 *
 * @param <C>
 */
public class WebServer<C> implements HttpHandler, WebSocketConnectionCallback {
	public static final String ioError = "There was an error processing your request";
	public static final String internalError = "There was an internal error processing your request";
	public static final String finalError = "Request processing failed";
	public final static ObjectMapper jackson = new ObjectMapper();
	private final ExecutorService worker;
	private final C context;
	private final Logger logger;
	private final List<String> prefixes;
	private final Map<String, RouteInfo> rawRoutes = new HashMap<>();

	private static class RouteInfo {
		final Class<? extends Request> request;
		final RouteConnection lifetime;
		final Method readMethod;

		private RouteInfo(
				Class<? extends Request> request, RouteConnection lifetime, Method readMethod
		) {
			this.request = request;
			this.lifetime = lifetime;
			this.readMethod = readMethod;
		}

		public Request read(Map<String, Deque<String>> queryParameters, InputStream body) {
			return uncheck(() -> (Request) readMethod.invoke(null, request, queryParameters, body));
		}
	}

	public WebServer(final C context, final ExecutorService worker, final String name, final List<String> prefixes) {
		this.context = context;
		this.worker = worker;
		logger = LoggerFactory.getLogger(String.format("server-%s", name));
		this.prefixes = prefixes;
	}

	/**
	 * Utility method to send message on websocket channel
	 *
	 * @param worker
	 * @param channel
	 * @param object
	 * @throws SuspendExecution
	 */
	public static void websocketSend(
			final ExecutorService worker, final WebSocketChannel channel, final Response object
	) throws SuspendExecution {
		final Coroutine coroutine = Coroutine.getActiveCoroutine();
		Coroutine.yieldThen(() -> {
			WebSockets.sendText(writeJson(object), channel, new WebSocketCallback<Void>() {
				@Override
				public void complete(final WebSocketChannel channel, final Void context1) {
					worker.submit(() -> {
						try {
							coroutine.process(null);
						} catch (final Throwable e) {
							com.zarbosoft.coroutines.Cohelp.fatal(worker, e);
						}
					});
				}

				@Override
				public void onError(
						final WebSocketChannel channel, final Void context1, final Throwable throwable
				) {
					worker.submit(() -> {
						try {
							coroutine.processThrow(Common.uncheckAll(throwable));
						} catch (final Throwable e) {
							com.zarbosoft.coroutines.Cohelp.fatal(worker, e);
						}
					});
				}
			});
		});
	}

	/**
	 * Utility method to serialize response to json
	 *
	 * @param response
	 * @return
	 */
	public static String writeJson(final Response response) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		uncheck(() -> response.write(stream));
		return new String(stream.toByteArray(), StandardCharsets.UTF_8);
	}

	/**
	 * Define an endpoint
	 *
	 * @param requestClass A Request class with RequestMeta annotation
	 * @return this, for chaining
	 */
	public WebServer<C> route(
			final Class<? extends Request> requestClass
	) {
		final RequestMeta meta = requestClass.getAnnotation(RequestMeta.class);
		if (meta == null)
			throw new Assertion(String.format("No meta: %s", requestClass));
		return route(requestClass, meta.path(), meta.ws());
	}

	/**
	 * Define an endpoint for a standard http request
	 *
	 * @param requestClass Request class, RequestMeta annotation ignored if present
	 * @param path         Route path
	 * @return
	 */
	public WebServer<C> route(
			final Class<? extends Request> requestClass, String path
	) {
		return route(requestClass, path, false);
	}

	/**
	 * Define an endpoint
	 *
	 * @param requestClass Request class, RequestMeta annotation ignored if present
	 * @param path         Route path
	 * @param ws           Is a websocket endpoint
	 * @return
	 */
	public WebServer<C> route(
			final Class<? extends Request> requestClass, String path, boolean ws
	) {
		Class<?>[] parameterTypes = new Class<?>[] {Class.class, Map.class, InputStream.class};
		Method read = null;
		Class checkIn = requestClass;
		while (checkIn != null) {
			try {
				read = checkIn.getMethod("read", parameterTypes);
				break;
			} catch (NoSuchMethodException e) {
			}
			checkIn = checkIn.getSuperclass();
		}
		rawRoutes.put(path, new RouteInfo(requestClass, ws ? RouteConnection.LONG : RouteConnection.SHORT, read));
		return this;
	}

	public Response handleInternal(
			final HttpString method, final ExchangeWrapper exchange, final WebSocketChannel channel
	) throws SuspendExecution {
		String path = exchange.getRelativePath();
		if (!prefixes.isEmpty()) {
			boolean prefixMatched = false;
			for (final String prefix : prefixes) {
				if (!path.startsWith(prefix))
					continue;
				path = path.substring(prefix.length());
				prefixMatched = true;
				break;
			}
			if (!prefixMatched)
				throw ErrorRequest.format(404, "Unknown request path %s", exchange.getRelativePath());
		}
		RouteInfo routeInfo = rawRoutes.get(path);
		if (routeInfo == null ||
				(channel == null && routeInfo.lifetime == RouteConnection.LONG) ||
				(channel != null && routeInfo.lifetime == RouteConnection.SHORT))
			throw ErrorRequest.format(404, "Unknown request path %s", exchange.getRelativePath());
		final Request request;
		try {
			request = exchange.read(routeInfo);
		} catch (final ValidationError e) {
			throw ErrorRequest.format("Error reading json: %s", e.getMessage());
		} catch (final IOException e) {
			// IOException == client not around to handle error
			throw new UncheckedIOException(e);
		}
		Response out = request.process(method, context, channel);
		if (out == null)
			out = ResponseEmpty.instance;
		return out;
	}

	@Override
	public void handleRequest(final HttpServerExchange exchange) throws Exception {
		exchange.startBlocking();
		exchange.dispatch(() -> {
			try {
				new Coroutine(() -> {
					try {
						try {
							exchange
									.getResponseHeaders()
									.put(HttpString.tryFromString("Access-Control-Allow-Origin"), "*");
							final Response result = handleInternal(exchange.getRequestMethod(), new ExchangeWrapper() {
								@Override
								public String getRelativePath() {
									return exchange.getRelativePath();
								}

								@Override
								public Request read(final RouteInfo requestInfo) throws IOException {
									final String length0 =
											exchange.getRequestHeaders().getFirst(Headers.CONTENT_LENGTH);
									final int length = length0 == null ? 0 : Integer.parseInt(length0);
									if (exchange.getRequestMethod().equals(Methods.GET) && length == 0) {
										return requestInfo.read(exchange.getQueryParameters(), null);
									} else {
										if (!(
												Methods.GET.equals(exchange.getRequestMethod()) ||
														Methods.POST.equals(exchange.getRequestMethod())
										))
											throw ErrorRequest.format("Unsupported HTTP method %s, must be GET or POST",
													exchange.getRequestMethod()
											);
										if (length == 0)
											return requestInfo.read(
													null,
													new ByteArrayInputStream("{}".getBytes(StandardCharsets.UTF_8))
											);
										else {
											return requestInfo.read(null, exchange.getInputStream());
										}
									}
								}
							}, null);
							exchange.setStatusCode(200);
							if (result.cache() != null)
								exchange.getResponseHeaders().put(Headers.CACHE_CONTROL,
										String.format("max-age=%s", result.cache().getSeconds())
								);
							result.write(exchange.getOutputStream());
						} catch (final ErrorRequest e) {
							exchange.setStatusCode(400);
							new ResponseError(e.code, e.getMessage()).write(exchange.getOutputStream());
						} catch (final Exception e) {
							logger.error(internalError, e);
							exchange.setStatusCode(500);
							new ResponseError(0, internalError).write(exchange.getOutputStream());
						} finally {
							exchange.endExchange();
						}
					} catch (final Exception e) {
						logger.warn(finalError, e);
					}
				}).process(null);
			} catch (final Throwable e) {
				com.zarbosoft.coroutines.Cohelp.fatal(worker, e);
			}
		});
	}

	@Override
	public void onConnect(final WebSocketHttpExchange exchange, final WebSocketChannel channel) {
		channel.getReceiveSetter().set(new AbstractReceiveListener() {
			@Override
			protected void onFullTextMessage(
					final WebSocketChannel channel, final BufferedTextMessage message
			) throws IOException {
				channel.getReceiveSetter().set(null);
				try {
					new Coroutine(() -> {
						try {
							try {
								final Response result = handleInternal(Methods.GET, new ExchangeWrapper() {
									@Override
									public String getRelativePath() {
										return URI.create(exchange.getRequestURI()).getPath();
									}

									@Override
									public Request read(final RouteInfo requestInfo) throws IOException {
										String data = message.getData();
										if (data.isEmpty())
											data = "{}";
										return requestInfo.read(
												null,
												new ByteArrayInputStream(data.getBytes(StandardCharsets.UTF_8))
										);
									}
								}, channel);
								websocketSend(worker, channel, result);
							} catch (final ErrorRequest e) {
								websocketSend(worker, channel, new ResponseError(e.code, e.getMessage()));
							} catch (final Exception e) {
								logger.error(internalError, e);
								websocketSend(worker, channel, new ResponseError(0, internalError));
							}
						} catch (final Exception e) {
							logger.warn(finalError, e);
						}
					}).process(null);
				} catch (final Throwable e) {
					com.zarbosoft.coroutines.Cohelp.fatal(worker, e);
				}
			}
		});
		channel.resumeReceives();
	}

	public Undertow start(final XnioWorker worker, final int port, final String server) {
		return Undertow
				.builder()
				.setServerOption(UndertowOptions.REQUIRE_HOST_HTTP11, false)
				.setWorker(worker)
				.addHttpListener(port, server)
				.setHandler(Handlers.websocket(this, new RequestBufferingHandler(this, 10)))
				.build();
	}

	private static enum RouteConnection {
		SHORT,
		LONG,
	}

	private abstract static class ExchangeWrapper {

		public abstract String getRelativePath();

		public abstract Request read(RouteInfo requestInfo) throws IOException;
	}
}
