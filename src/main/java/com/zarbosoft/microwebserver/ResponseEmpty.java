package com.zarbosoft.microwebserver;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import static com.zarbosoft.rendaw.common.Common.uncheck;

@JsonSerialize(using = ResponseEmpty.Serializer.class)
public class ResponseEmpty extends Response {
	public static ResponseEmpty instance = new ResponseEmpty();

	public ResponseEmpty() {

	}

	public static class Serializer extends JsonSerializer<ResponseEmpty> {
		@Override
		public void serialize(
				final ResponseEmpty value, final JsonGenerator gen, final SerializerProvider serializers
		) throws IOException {
		}
	}

	@Override
	public void write(OutputStream stream) throws IOException {
		stream.write("{}".getBytes(StandardCharsets.UTF_8));
	}
}
