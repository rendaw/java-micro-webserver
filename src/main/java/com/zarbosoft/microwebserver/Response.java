package com.zarbosoft.microwebserver;

import java.io.IOException;
import java.io.OutputStream;
import java.time.Duration;

import static com.zarbosoft.microwebserver.WebServer.jackson;
import static com.zarbosoft.rendaw.common.Common.uncheck;

public abstract class Response {
	public Duration cache() {
		return null;
	}

	public void write(OutputStream stream) throws IOException {
		jackson.writeValue(stream, this);
	}
}
