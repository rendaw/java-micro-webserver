package com.zarbosoft.microwebserver;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseErrorError {
	@JsonProperty(value = "code")
	public final int code;
	@JsonProperty(value = "message")
	public final String message;

	@JsonCreator
	public ResponseErrorError(
			@JsonProperty(value = "code") final int code, @JsonProperty(value = "message") final String message
	) {
		this.code = code;
		this.message = message;
	}
}
