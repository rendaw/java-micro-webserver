package com.zarbosoft.microwebserver;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public abstract class ResponseFreeJson extends Response {
	public abstract void write(JsonGenerator body);

	@Override
	final public void write(OutputStream stream) throws IOException {
		try (final JsonGenerator bodyWriter = new JsonFactory().createGenerator(stream)) {
			write(bodyWriter);
		}
	}
}
