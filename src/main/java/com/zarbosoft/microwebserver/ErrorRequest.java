package com.zarbosoft.microwebserver;

public class ErrorRequest extends RuntimeException {
	public final int code;

	public static final int outwardCodeBadTOS;
	public static final int outwardCodeMissing;
	public static final int invalidInput;

	static {
		int i = 1;
		outwardCodeBadTOS = i++;
		outwardCodeMissing = i++;
		invalidInput = i++;
	}

	public ErrorRequest(final int code, final String message) {
		super(message);
		this.code = code;
	}

	public static ErrorRequest format(final String template, final Object... args) {
		return new ErrorRequest(400, String.format(template, args));
	}

	public static ErrorRequest format(final int code, final String template, final Object... args) {
		return new ErrorRequest(code, String.format(template, args));
	}

	public static ErrorRequest literal(final String message) {
		return new ErrorRequest(400, message);
	}

	public static ErrorRequest literal(final int code, final String message) {
		return new ErrorRequest(code, message);
	}
}
