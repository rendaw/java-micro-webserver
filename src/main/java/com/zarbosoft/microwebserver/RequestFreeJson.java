package com.zarbosoft.microwebserver;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.InputStream;
import java.util.Deque;
import java.util.Map;

import static com.zarbosoft.microwebserver.WebServer.jackson;
import static com.zarbosoft.rendaw.common.Common.uncheck;

public abstract class RequestFreeJson<C> extends Request<C> {
	public JsonNode data;

	public static Request read(Class klass, Map<String, Deque<String>> queryParameters, InputStream body) {
		return uncheck(() -> {
			RequestFreeJson out = (RequestFreeJson) klass.newInstance();
			out.data = jackson.readTree(body);
			return out;
		});
	}
}
