package com.zarbosoft.microwebserver;

import com.google.common.io.BaseEncoding;
import com.zarbosoft.checkjson.CheckJson;
import com.zarbosoft.checkjson.Valid;
import com.zarbosoft.coroutinescore.SuspendExecution;
import com.zarbosoft.rendaw.common.Assertion;
import io.undertow.util.HttpString;
import io.undertow.websockets.core.WebSocketChannel;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;

import static com.zarbosoft.rendaw.common.Common.uncheck;

/**
 * Defines an endpoint. Deserialized from request parameters or else body of request, and validated with checkjson.
 *
 * @param <C> context
 */
public abstract class Request<C> {
	/**
	 * Called after instantiating with extra request data.
	 * <p>
	 * If a standard http request, the returned value will be sent as a response.  If a websocket request, the returned
	 * value will be sent as an initial response but the websocket will remain open.
	 *
	 * @param method
	 * @param context
	 * @param websocket null if request is not a websocket request
	 * @return The response
	 * @throws SuspendExecution
	 */
	public abstract Response process(
			HttpString method, C context, WebSocketChannel websocket
	) throws SuspendExecution;

	public static Request read(Class klass, Map<String, Deque<String>> queryParameters, InputStream body) {
		if (queryParameters != null) {
			final Object out = uncheck(() -> klass.getConstructor().newInstance());
			for (final Field field : klass.getFields()) {
				final String raw = queryParameters.getOrDefault(field.getName(), new ArrayDeque<>()).peekFirst();
				if (raw == null) {
					final Valid valid = field.getAnnotation(Valid.class);
					if (valid != null) {
						if (!valid.optional())
							throw ErrorRequest.format("Missing query parameter [%s]", field.getName());
					}
					continue;
				}
				final Object value;
				if (field.getType() == Long.class || field.getType() == long.class) {
					value = Long.parseLong(raw);
				} else if (field.getType() == Integer.class || field.getType() == int.class) {
					value = Integer.parseInt(raw);
				} else if (field.getType() == String.class) {
					value = raw;
				} else if (field.getType() == byte[].class) {
					value = BaseEncoding.base64().decode(raw);
				} else
					throw new Assertion();
				uncheck(() -> field.set(out, value));

			}
			return (Request) out;
		} else {
			return uncheck(() -> (Request) CheckJson.read(body, klass));
		}
	}
}
