package com.zarbosoft.microwebserver;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseError extends Response {
	@JsonProperty(value = "error")
	public final ResponseErrorError error;

	public ResponseError(final int code, final String message) {
		error = new ResponseErrorError(code, message);
	}

	@JsonCreator
	public ResponseError(@JsonProperty(value = "error") final ResponseErrorError error) {
		this.error = error;
	}
}
