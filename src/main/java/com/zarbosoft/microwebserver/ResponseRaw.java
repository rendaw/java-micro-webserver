package com.zarbosoft.microwebserver;

import java.io.IOException;
import java.io.OutputStream;
import java.time.Duration;

public class ResponseRaw extends Response {
	public final Duration cache;
	public final byte[] data;

	public ResponseRaw(final Duration cache, final byte[] data) {
		this.cache = cache;
		this.data = data;
	}

	@Override
	public void write(OutputStream stream) throws IOException {
		stream.write(data);
	}
}
