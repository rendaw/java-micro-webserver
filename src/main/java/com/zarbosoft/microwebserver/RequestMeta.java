package com.zarbosoft.microwebserver;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Provides metadata about an endpoint to the webserver.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestMeta {
	/**
	 * True if the endpoint is a websocket endpoint
	 *
	 * @return
	 */
	boolean ws() default false;

	/**
	 * Endpoint path
	 *
	 * @return
	 */
	String path();
}
